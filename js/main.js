const TURN_COMPUTER = 0;
const TURN_PLAYER = 1;

var vm = new Vue({
    el: '#app',
    data: {
        isRunning: false,
        numGames: 0,
        computerColors: [],  // array de colores que genera el ordenador
        playerColors: [],    // array de colores que va pulsando el jugador
        turn: TURN_COMPUTER, 
        currentComputerColor: -1,  // color actual en el visor
        numPushedPlayerColors: -1,  // número de colores pulsados por el usuario
        error: false,
        record: 0
    },
    audioContext: null,
    computed: {
        txtBtnRun: function() {
            return this.numGames > 0 ? "Otra partida" : "Empezar";
        },
        currentComputerColorClass: function() {
            return {
                red: this.currentComputerColor === 0,
                blue: this.currentComputerColor === 1,
                green: this.currentComputerColor === 2,
                yellow: this.currentComputerColor === 3
            }
        }
    },
    watch: {
        numPushedPlayerColors: function(val) {
            this.record = Math.max(this.record, val);
        }
    },
    methods: {
        /* Arranco el juego */
        run: function() {            
            this.isRunning = true;
            this.numGames++;
            this.computerColors = [];
            this.turn = TURN_PLAYER;
            this.error = false;
            this.computerTurn();
         },
         /* Paro el juego */
         stop: function() {
            this.isRunning = false;
            this.turn = TURN_COMPUTER;
         },
         /* genera un color aleatorio y lo introduce en la lista de colores a mostrar */
         generateColor: function() {
            var color = Math.floor((Math.random() * 4));
            this.computerColors.push(color);
        },
        /* Cambio de turno */
        changeTurn: function() {
            if (this.isRunning === false) return; // si no está en marcha no se hace el cambio
            this.turn = 1 - this.turn;
        },
        /* gestiona el turno del ordenador */
        computerTurn: function() {
            this.changeTurn();
            this.generateColor();
            this.showColorSequence(0);  // muestro la secuencia de colores empezando por el 0
        },
        /* gestiona el turno del jugador */
        playerTurn: function() {
            this.changeTurn();
            this.playerColors = [];
            this.currentComputerColor = -1; // mientras turno jugador no se debe ver ningún color en visor
            this.numPushedPlayerColors = 0;
        },
        /* Muestra un color de la lista de colores del ordenador */
        showColorSequence: function(idx) {
            if (this.isRunning === false) return; // si no está en marcha no se cambia el color
            
            // no hay más colores que mostrar
            if (idx === this.computerColors.length) {
                this.playerTurn(); // turno jugador
                return;
            }
            // si es el mismo que antes quita color y espera un poco antes de poner el actual
            if (idx > 0 && this.computerColors[idx] === this.computerColors[idx-1]) {
                this.currentComputerColor = -1;
                setTimeout(() => {
                    this.showComputerColor(idx);
                }, 110);
            } else {
                this.showComputerColor(idx);
            }
        },
        /* Muestra el nuevo color y llama al siguiente */
        showComputerColor: function(idx) {
            this.currentComputerColor = this.computerColors[idx];
            this.playSound(this.currentComputerColor);

            setTimeout(() => {
                this.showColorSequence(++idx)
            }, 800);  
        },
        /* Gestiona el click sobre un botón */
        clickColor: function(color) {
            if (this.turn == TURN_COMPUTER) return;
            this.playSound(color);
            if (this.computerColors[this.numPushedPlayerColors] != color) {
                this.error = true;
                this.stop();
            } else {
                if (this.numPushedPlayerColors + 1 === this.computerColors.length) {
                    this.addPlayerColor(color);
                    // espero un segundo antes de pasa el turno
                    // para poder ver el ultimo color pulsado
                    setTimeout(() => { 
                        this.computerTurn();
                    }, 1000);
                } else {
                    this.addPlayerColor(color);
                }
            }
        },
        /* Añade el color pulsado a la lista de colores */
        addPlayerColor: function(color) {
            var newcolor =  {
                idx: this.currentPlayerColor,
                color: color
            }
            this.playerColors.push(newcolor);
            this.numPushedPlayerColors++;
        },
        /* Hace sonar una nota en función del color */
        playSound: function(color) {
            var oscilator = this.$options.audioContext.createOscillator()
            var gain = this.$options.audioContext.createGain()
            oscilator.type = "sine";
            switch (color)
            {
                case 0:
                    oscilator.frequency.value = 261.6; // C4
                    break;
                case 1:
                    oscilator.frequency.value = 329.6; //E4
                    break;
                case 2:
                    oscilator.frequency.value = 392.0; //G4
                    break;
                case 3:
                    oscilator.frequency.value = 523.3; //C5
                    break;
            }
            
            oscilator.connect(gain)
            gain.connect(this.$options.audioContext.destination)
            oscilator.start(0)
            gain.gain.exponentialRampToValueAtTime(
                0.00001, this.$options.audioContext.currentTime + 0.5
            )
        }
    },
    created() {
        this.$options.audioContext = new AudioContext();
    }
}); 
